from pyms import Datalogger

PORT = '/dev/ttyUSB0'
BAUDRATE = 115200
MODEL = "CR1000"

CR1000 = Datalogger(PORT, BAUDRATE, MODEL)

# supported datalogger list
print(CR1000.supported_datalogger())

# start connection
CR1000.start()

# save stored data on a file
CR1000.get_stored_data("data.txt")

# get a realtime data
realtime_data = CR1000.get_realtime_data(self)
print(realtime_data)

# save realtime data capture during a 2 hours period
# 0 day, 2 hours, 0 minutes, 5 hz, 3 minutes of data by file, dest path
CR1000.save_realtime_period(0, 2, 0, 5, 3, "/home/emi/realtime_capture/")
