PyMS
====

Software para automatizar la gestión de la estación climática Pegasus - Tecmes.

Funcionamiento
--------------

Extrae los datos haciendo OCR sobre la pantalla del software de monitoreo de la Pegasus.

Dependencias
------------

**tesseract**

La última versión probada 3.05.01 (https://digi.bib.uni-mannheim.de/tesseract/tesseract-ocr-setup-3.05.01.exe)

https://github.com/tesseract-ocr/tesseract/wiki

**Anaconda**

La rama para python 2.7 y 32 bits. La versión 2.5.1.0.

https://www.anaconda.com/download/

**pytesseract**

Pytesseract es un wrapper para python.
 https://github.com/madmaze/pytesseract

Se instala haciendo:


``pip install pytesseract``

**PyAutoGUI**

Para automatizar tareas de escritorio.
https://github.com/madmaze/pytesseract

``pip install PyAutoGUI``


Test de funcionamiento
----------------------

Una vez instalado todo, debería funcionar esto:

.. code:: python

    try:
        import Image
    except ImportError:
        from PIL import Image
    import pytesseract

y también esto otro:

.. code:: python

    print(pytesseract.image_to_string(Image.open('test.png')))
