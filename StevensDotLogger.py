from pyms import Datalogger

PORT = '/dev/ttyUSB0'
BAUDRATE = 115200
MODEL = "DOTLOGGER"

Stevens = Datalogger(PORT, BAUDRATE, MODEL)

# start connection
Stevens.start()

# get a realtime data
realtime_data = Stevens.get_realtime_data()
print(realtime_data)
