# -*- coding: utf-8 -*-
from __future__ import print_function
from win32api import GetSystemMetrics
from datetime import datetime
from pyms import calc

import glob
import os
import time
import logging
import requests
import pyautogui
import pytesseract

resolucion = GetSystemMetrics(0), GetSystemMetrics(1)

def test_capture():
    if resolucion != (1280,1024):
        print("Cambio de resolucion")
        return

    # recuadros de captura (x0,y0,ancho,alto)
    screenshot_box = (400, 310, 1150-400, 800-310)
    # recortes x variable (x0, y0, x1, y1)
    boxes = {"temp":(62,134,130,170),
             "hume":(335,134,387,170),
             "pATM":(288,24,387,61),
             "direViento": (336,330,418,368),
             "veloViento":(344,433,402,466)}
    # nombre de captura de imagen
    timestamp = datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    fn = (timestamp.replace(':', '-')).replace(' ', '_')
    imgfilename = fn + '.jpg'
    # captura y guarda imagen
    img = pyautogui.screenshot(imgfilename, region=screenshot_box)
    # un archivo mensual: YYYY-MM.CSV
    fn_archi_datos = datetime.fromtimestamp(time.time()).strftime('%Y-%m')+'.CSV'
    sep = '\t'

    # recorto y guardo cada subimagen
    # img.crop(boxes[var]).save('{}.png'.format(var))

    # OCR de valores con el mismo orden que el header
    valores_ocr = {variable:pytesseract.image_to_string(img.crop(box), config="-psm 7") for variable, box in boxes.items()}

    # escribe cabecera si el archivo no existe
    header_csv = ["fecha"] + [variable for variable in sorted(valores_ocr)] + ["STermica"]
    if not os.path.isfile(fn_archi_datos):
        archi_datos = open(fn_archi_datos,'a')
        for k in header_csv:
            archi_datos.write(k)
            archi_datos.write(sep)
        archi_datos.write('\n')
    else:
        archi_datos = open(fn_archi_datos,'a')


    # conversion a valores flotantes menos direViento pq es str
    valores_limpios = {variable:float(valor) for variable, valor in valores_ocr.items() if variable != "direViento"}
    # paso direViento tal cual es
    valores_limpios["direViento"] = valores_ocr["direViento"]
    # agrego timestamp
    valores_limpios["fecha"] = timestamp
    # calculo STermica segun SMN
    T, H, V = valores_limpios['temp'], valores_limpios['hume'], valores_limpios['veloViento']
    valores_limpios["STermica"] = calc.temp2STSMN(T, H, V)

    for k in header_csv:
         archi_datos.write(str(valores_limpios[k]))
         archi_datos.write(sep)
    archi_datos.write('\n')
    print("ocr=", valores_ocr)
    print("limpios= ",valores_limpios)



if __name__ == '__main__':

    test_capture()
