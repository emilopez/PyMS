# -*- coding: utf-8 -*-


def calib_pat(presion, umbral=1005, offset=1.3):
    '''Calibración presion atm x CIM'''
    if presion >= umbral:
        presion -= offset
    return presion


def tempF2C(F):
    '''Conversión de F a C'''
    return (5.0 / 9.0) * F - 32.0


def tempC2F(C):
    '''Conversión de C a F'''
    return (9.0 / 5.0) * C + 32


def temp2ST(Tc, H, V):
    '''Conversión de temp a sensación térmica

        input:
            - Tc: temperatura [C]
            - H: humedad [%]
            - V: velocidad de viento [km/h]

        return stC (sensación térmica en C):
            - x CALOR: (Tc >= 26) and (H >= 40)
            - x FRIO: (Tc <= 10) and ( V >= 6)
            - None (no se calcula st)
    '''
    stC = None
    if (Tc >= 26) and (H >= 40):
        F = tempC2F(Tc)
        stF = -42.379 \
             + (2.04901523 * F) \
             + (10.14333127 * H) \
             - (0.22475541 * F * H) \
             - (6.83783 * (10 ** -3) * (F ** 2)) \
             - (5.481717 * (10 ** -2) * (H ** 2)) \
             + (1.22874 * (10 ** -3) * (F ** 2) * H) \
             + (8.5282 * (10 ** -4) * F * (H ** 2)) \
             - (1.99 * (10 ** -6) * (F ** 2) * (H ** 2))
        stC = round(tempF2C(stF), 1)

    if (Tc <= 10) and (V >= 6):
        t1 = V * 0.6214 * 0.447
        stC = 33.0 \
            + (Tc - 33.0) \
            * (0.474 + 0.474 * ((t1 - 0.0454 * t1) ** 0.5))
        stC = round(stC, 1)

    return stC


def temp2STSMN(TT, hr, V):
    c = 0
    stn = None
    # ST x calor
    if (TT >= 26) and (hr >= 40):
        hi = -8.78469476 \
            + 1.61139411 * TT \
            + 2.338548839 * hr \
            - 0.14611605 * TT * hr \
            - 0.012308094 * (TT ** 2) \
            - 0.016424828 * (hr ** 2) \
            + 0.002211732 * (TT ** 2) * hr \
            + 0.00072546 * TT * (hr ** 2) \
            - 0.000003582 * (TT ** 2) * (hr ** 2)
        if (V >= 50):
            c = 0.00016714 * (TT ** 4) \
            - 0.026379 * (TT ** 3) \
            + 1.521 * (TT ** 2) \
            - 37.614 * TT \
            + 334.04
        if ((V < 50) and (V >= 36)):
            c = 0.00011707 * (TT ** 4) \
            - 0.018359 * (TT ** 3) \
            + 1.049 * (TT ** 2) \
            - 25.593 * TT \
            + 222.51
        if ((V < 36) and (V >= 21.5)):
            c = -0.000032676 * (TT ** 4)\
            + 0.0045647 * (TT ** 3) \
            - 0.24233 * (TT ** 2) \
            + 5.9769 * TT - 59.076
        if ((V < 21.5) and (V >= 12.5)):
            c = -0.00003652 * (TT ** 4) \
            + 0.0060094 * (TT ** 3) \
            - 0.36769 * (TT ** 2) \
            + 9.9097 * TT \
            - 99.223
        stn = round(hi + c, 1)
    # ST x frio
    if (TT <= 10) and (V >= 6):
        stn = 33.0 \
            + (TT - 33.0) \
            * (0.474 + 0.474 * ((V * 0.6214 * 0.447 - 0.0454 * V * 0.6214 * 0.447) ** 0.5))
        stn = round(stn, 1)
    return stn
