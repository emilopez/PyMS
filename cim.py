# -*- coding: utf-8 -*-

from __future__ import print_function
from datetime import datetime

import glob
import os
import time
import logging
import requests

from pyms import Pegasus
try:
    import pyautogui
except:
    print('no carga pyautogui')


def test9():
    ''' recorto la imagen, guardo los recortes y hago OCR de cada una'''
    import pytesseract
    from PIL import Image

    dl = Pegasus()

    var = ['pat', 'tae', 'hae', 'pad', 'ddv', 'vdv']
    out = []
    img = Image.open('2015-10-06_17-22-49.jpg')
    j = 0
    for im in dl.crop_image(img):
        out.append(pytesseract.image_to_string(im, lang='spa', config="-psm 7"))
        im.save(var[j] + '.jpg')
        j += 1
    print(out)


def main(im=None):

    dl = Pegasus()
    try:
        rtdata = dl.getrealtime(im)
        dl.saverealtime()
        payload = dl.getdataRTS()
        print('Captura de datos: OK')
    except:
        logging.info('Captura de datos: Error')
        print('Captura de datos: Error. intento de cerrar ventana.')
        pyautogui.click(855,428)
    try:
        # envio datos
        r = requests.post("http://fich.unl.edu.ar/apicim/index.php/estacion/", json=payload)
        logging.debug(str(r.status_code) + ' ' + r.text)
        print("To FICH:", str(r.status_code))
        print(rtdata)
        print()
    except:
        print('Envio fich: Error, ver cim.log')
        
    try:
        # envio datos
        r = requests.get("http://cim.yosobreip.com.ar/feed.php", params=payload)
        logging.debug(str(r.status_code) + ' ' + r.text)
        print("To yosobreip:", str(r.status_code))
        print(rtdata)
        print()
    except:
        print('Envio yosobreip: Error, ver cim.log')


if __name__ == '__main__':


    logging.basicConfig(filename='cim.log', format='%(asctime)s %(message)s',
        level=logging.DEBUG)
    main()    
    #while True:
    #    delay = 10  # minutos
        #main()
        #print('Proximo captura en: ', delay, "minutos")
        #print()
        #except:
        #    logging.info('Error en captura, intento de cerrar posible ventana')
        #    print('Error en captura, intento de cerrar posible ventana')
        #    #pyautogui.hotkey('alt','f4')
        #    #pyautogui.moveTo(883,484)
        #    pyautogui.click(855,428)
                            
        #time.sleep(delay*60)
        #timestamp = int(datetime.fromtimestamp(time.time()).strftime('%H'))
