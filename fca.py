# -*- coding: utf-8 -*-

from __future__ import print_function
from datetime import datetime

import glob
import os
import time
import logging
import requests

from pyms import Pegasus

try:
    import pyautogui
except:
    print('no carga pyautogui')

try:
    import pytesseract
except:
    print('no carga pytesseract')

try:
    from win32api import GetSystemMetrics
    width = GetSystemMetrics(0)
    hight = GetSystemMetrics(1)
except:
    print('no carga win32api')

def main(im=None):

    dl = Pegasus()
    dl.screen_resolution = (width, hight)
    if dl.screen_resolution != (1280,1024):
        print("Cambio de resolucion")
        return

    pyautogui.click(816,555)
    time.sleep(1)
    dl.screenshot_box = (397, 330, 570, 400) # zona de recorte (inix, iniy, widthx, highty)
    dl.screenshot() # caputra pantalla en screenshot_box
    dl.acron["ste"] = u"Sensacion termica"#
    dl.acron["ddv"] = u"Direccion del viento"#
    dl.acron["tds"] = u"Temperatura de suelo"
    dl.acron["hae"] = u"Humedad ambiente exterior"#
    dl.acron["idr"] = u"Intensidad de rafaga"
    dl.acron["tai"] = u"Temperatura ambiente interior"
    dl.acron["pur"] = u"Punto de rocio"
    dl.acron["hds"] = u"Humedad de suelo"
    dl.acron["vdv"] = u"Velocidad de viento"#
    dl.acron["pad"] = u"Precipitacion acumulada diaria"#
    dl.acron["pat"] = u"Presion atmosferica"#
    dl.acron["tae"] = u"Temperatura ambiente exterior"#
    dl.acron["hai"] = u"Humedad ambiente interior"
    dl.acron["ddr"] = u"Direccion de rafaga"
    dl.acron["ras"] = u"Radiacion solar"

    # box coord c/variable: (x0,y0,x1,y1)
    dl.variables_box = [ {"var": "tai", "box":(0,0,80,40)}, {"var": "pat", "box":(180,0,280,40)}, {"var": "hai", "box":(435,0,485,40)},
         {"var": "tae", "box":(0,100,80,130)}, {"var": "hae", "box":(225,100,280,130)}, {"var": "pad", "box":(415,100,480,130)},
         {"var": "tds", "box":(0,185,80,220)}, {"var": "hds", "box":(225,185,280,220)}, {"var": "ras", "box":(415,185,480,220)},
         {"var": "ste", "box":(0,274,80,308)}, {"var": "ddv", "box":(180,274,340,308)}, {"var": "ddr", "box":(385,274,535,308)},
         {"var": "pur", "box":(0,360,80,395)}, {"var": "vdv", "box":(225,360,293,395)}, {"var": "idr", "box":(415,360,495,395)}]

    dl.crop_variables()
    dl.ocr_set_raw_variables()
    dl.set_clean_data()
    dl.set_calibrate_data()

    #for k, v in dl.rtdata.items():
    #    print(k, v["raw_data"], v["cls_data"], v["cal_data"])

    dl.save_data()
    payload = dl.set_data_RTS()
    for k, v in payload.items():
        print(k,v)

    # envio datos
    r = requests.get("http://fca.yosobreip.com.ar/feed.php", params=payload)
    logging.debug(str(r.status_code) + ' ' + r.text)
    print("To yosobreip:", str(r.status_code))


    '''
    print('Captura de datos: OK')
    '''

def test_crop_val():
    from PIL import Image

    dl = Pegasus()
    #box son las coord x0,y0,x1,y1
    variables = [ {"var": "tai", "box":(0,0,80,40)}, {"var": "pat", "box":(180,0,280,40)}, {"var": "hai", "box":(435,0,485,40)},
         {"var": "tae", "box":(0,100,80,130)}, {"var": "hae", "box":(225,100,280,130)}, {"var": "pad", "box":(415,100,480,130)},
         {"var": "tds", "box":(0,185,80,220)}, {"var": "hds", "box":(225,185,280,220)}, {"var": "ras", "box":(415,185,480,220)},
         {"var": "ste", "box":(0,274,80,308)}, {"var": "ddv", "box":(180,274,340,308)}, {"var": "ddr", "box":(385,274,535,308)},
         {"var": "pur", "box":(0,360,80,395)}, {"var": "vdv", "box":(225,360,293,395)}, {"var": "idr", "box":(415,360,495,395)},
    ]

    img = Image.open('test/fca_cropped.jpg')
    for v in variables:
        i = img.crop(v["box"])
        i.save(v["var"]+".png")


if __name__ == '__main__':

    logging.basicConfig(filename='fca.log', format='%(asctime)s %(message)s', level=logging.DEBUG)
    main()
    #test_crop_val()
