# -*- coding: utf-8 -*-
'''
    PyPegasus
    ----------------

    The public API and command-line interface to PyPegasus.

    :copyright: Copyright 2015 Emiliano López et al, see AUTHORS.
    :license: GNU GPL v3.

'''
from .device import Pegasus
#from .datalogger import OpenDatalogger

VERSION = '0.4dev'
__version__ = VERSION
