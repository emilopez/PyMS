import serial
from datetime import *
import time
import logging


class Datalogger:

     #defino elemento de loggeo de python
    logging.basicConfig(filename='syslog.log', format='%(asctime)s - %(levelname)s - %(message)s', level=logging.DEBUG)

    def __init__(self, port, baudrate, model, parity=serial.PARITY_NONE, rtscts=0, xonxoff=0):
        self.commands = {"CR3000":{"cmdRT":"7", "cmdData":"8"}, "CR1000":{"cmdRT":"7", "cmdData":"8"}, "DOTLOGGER":{"cmdRT":"CD", "cmdData":"DB"}}
        self.set_model(model)
        try:
            self.serial = serial.Serial(port, baudrate, parity=parity, rtscts=rtscts, xonxoff=xonxoff)
        except serial.serialutil.SerialException:
            logging.error("Serial port connection error")


    def supported_datalogger(self):
        '''return the list of supported dataloggers'''
        return [i for i in self.commands]

    def set_model(self, model):
        '''set the datalogger model and its commands'''
        if model in self.supported_datalogger():
            self.model = model
            self.cmdRT = self.commands[self.model]["cmdRT"]
            self.cmdData = self.commands[self.model]["cmdData"]
        else:
            sms = "Not supported datalogger"
            logging.error(sms)
            print(sms)


    def start(self):
        '''Start serial connection'''
        try:
            self.serial.open()
        except self.serial.SerialException as e:
            logging.error("No se puedo abrir el puerto %s: %s\n" % (self.serial.portstr, e))
        logging.info("Communication was started")

    def close(self):
        '''Close serial connection'''
        self.serial.close()
        logging.info("Communication was closed")

    # Permite cambiar el puerto y la velocidad de la comunicacion
    def set_parameters(self,port, baudrate):
        self.serial = serial.Serial(port, baudrate)
        logging.info("Settings were updated")


    # Devuelve por pantalla los datos en tiempo real de equipo
    #def get_data_rt(self):
    def get_realtime_data(self):
        '''get and return realtime data'''

        logging.info("Inicio Obtencion tiempo real")
        # mientras el otro extremo de la conexion no responda mandamos ENTER
        while self.serial.inWaiting() == 0:
            self.serial.write('\r\n')
            time.sleep(1)
        # enviamos en numero 7 para que el datalogger nos devuelva los datos actuales
        self.serial.write(self.cmdRT+'\r\n')
        out = ''
        # se espera un segundo antes de leer la salida
        time.sleep(1)
        while self.serial.inWaiting() > 0:
            out += self.serial.read(1)
        if out != '':
            return out

    def save_realtime_period(self, dd, hh, mm, freq, filezise, pathFile):
        ''' Save realtime data during a period of time with a specific frequency sampling

            dd, hh, mm: period of quering the datalogger for realtime data
            freq: 1 to 8 hertz
            filezise: in minutes, to change the file to store (each file stores X minutes of records)
            pathFile: path of the files
        '''
       # dependiendo la cantidad de muestras saco el tiempo de espera
        logging.info("Inicio Captura tiempo real")
        freq = float(freq)
        dd = int(dd)
        hh = int(hh)
        mm = int(mm)
        filezise = int(filezise)
        tiempo = round(1.0/freq,3) # // division con resultado redondeado
        while self.serial.inWaiting() == 0:
            self.serial.write('\r\n')
            time.sleep(1)
        # tomo la fecha y hora de arranque
        fhInicio = (datetime.today())
        restaDias = 0
        restaHoras = 0
        restaMinutos = 0
        # este ciclo es el que controla la cantidad de tiempo que dura la ejecucion de la funcion
        while not((restaDias == dd) and (restaHoras == hh) and (restaMinutos == minutos)):
            # tomo fecha y hora actual para controlar la duracion de la funcion
            fhActual=(datetime.today())
            # se crea el archivo
            nombreArchi=str(fhActual)
            try:
                 f = open(pathFile+nombreArchi+".raw", "w")
            except IOError:
                logging.error("File couldn't be created")
            # pone en cero la variable
            restaMinutos2=0
            out=''
            while self.serial.inWaiting() == 0:
                self.serial.write('\r\n')
                time.sleep(1)
            # este segundo ciclo controla la duracion de cada archivo de consulta(mediante tiempo)
            while not(restaMinutos2 == filezise):
                # tomo fecha y hora actual para controlar la duracion del archivo
                fhActual2=(datetime.today())
                # -------se realiza la consulta al datalogger--------
                self.serial.write(self.cmdRT+'\r\n')
                # se espera- tiempo- antes de leer la salida
                time.sleep(tiempo)
                # devuelve la cantidad de chars en el buffer
                # lee esa cantidad de caracteres
                cantChar=int(self.serial.inWaiting())
                if (cantChar > 0):
                    out = self.serial.read(cantChar)
                    #print out
                # cada consulta se agrega al archivo
                if out != '':
                    f.write("Timestamp:" + str(fhActual2))
                    f.write(out)
               # se limpian los buffer de la comunicacion serie
                self.serial.flush()
                self.serial.flushInput()
                self.serial.flushOutput()
                #--------------        --------------------
                # control segundo ciclo
                # realizo la resta de la fecha actual y la inicial, devuelve un objeto timedelta
                resta2=fhActual2-fhActual
                restaMinutos2 = int(resta2.seconds // 60 % 60)
            # cuando se cumple el tiempo de duracion del archivo se cierra
            f.close()
            # control primer ciclo
            # realizo la resta de la fecha actual y la inicial, devuelve un objeto timedelta
            fhActual3=(datetime.today())
            resta=fhActual3-fhInicio
            # del objeto saco los dias, las horas y los minutos, estos se encuentran en segundos
            restaDias= int(resta.days)
            restaHoras=int(resta.seconds // 3600)
            restaMinutos=int(resta.seconds // 60 % 60)

    def get_stored_data(self,pathFile):
        logging.info("Start getting stored Flux table data")
        while self.serial.inWaiting() == 0:
            self.serial.write('\r\n')
            time.sleep(1)
        # se crea el archivo con la fecha y hora
        fhActual=(str(datetime.today()))
        # le manda el caracter 8 para obtener las talbas almacenadas en el datalogger
        self.serial.write(self.cmdData+'\r\n')
        out= ''
        # se espera un segundo antes de leer la salida
        time.sleep(1)
        # luego almacena en out todo lo que le devuelve el datalogger

        # devuelve la cantidad de chars en el buffer
        # lee esa cantidad de caracteres
        cantChar=int(self.serial.inWaiting())
        if (cantChar > 0):
            out = self.serial.read(cantChar)
        if (out != '') & ("flux" in out ):
            try:
                f = open(pathFile+"flux"+fhActual+".raw", "w")
            except IOError:
                logging.error("File couldn't be created")
            while (out != ''):
                if("ts_data" in out):
                    f.write(out)
                    logging.info("ts_data flag received")
                    break
                else:
                    f.write(out)
                # se envian un par de enter para obtener respuesta del datalogger
                while self.serial.inWaiting() == 0:
                    self.serial.write('\r\n')
                    time.sleep(1)
                #le manda el caracter 8 para obtener las talbas almacenadas en el datalogger
                self.serial.write(self.cmdData+'\r\n')
                #se espera un segundo antes de leer la salida
                time.sleep(1)
                # luego almacena en out todo lo que le devuelve el datalogger
                # devuelve la cantidad de chars en el buffer
                # lee esa cantidad de caracteres
                cantChar=int(self.serial.inWaiting())
                if (cantChar > 0):
                    out = self.serial.read(cantChar)
            self.serial.flush()
            self.serial.flushInput()
            self.serial.flushOutput()
            f.close()
        else:
            logging.info("No stored data")
