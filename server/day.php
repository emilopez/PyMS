<?php
include_once("conn.php");
include_once("funcionesdb.php");


$day = $_REQUEST['d'];
$valores = dia($day);

$style = "<style type=\"text/css\">
#table-3 {
	border: 1px solid #DFDFDF;
	background-color: #F9F9F9;
	width: 100%;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
	font-family: Arial,\"Bitstream Vera Sans\",Helvetica,Verdana,sans-serif;
	color: #333;
}
#table-3 td, #table-3 th {
	border-top-color: white;
	border-bottom: 1px solid #DFDFDF;
	color: #555;
}
#table-3 th {
	text-shadow: rgba(255, 255, 255, 0.796875) 0px 1px 0px;
	font-family: Georgia,\"Times New Roman\",\"Bitstream Charter\",Times,serif;
	font-weight: normal;
	padding: 7px 7px 8px;
	text-align: left;
	line-height: 1.3em;
	font-size: 14px;
}
#table-3 td {
	font-size: 12px;
	padding: 4px 7px 2px;
	vertical-align: top;
}
</style>";

$header = $style."<table id=\"table-3\">
<thead>
<th colspan=\"9\"><b>Datos del día:".$day."</b></th>
</thead>
		<thead>
			<th>Id</th>
			<th>Datetime</th>
			<th>Sensación Térmica</th>
			<th>Presión atmosférica</th>
			<th>Velocidad del Viento</th>
			<th>Dirección del Viento</th>
			<th>Humedad</th>
			<th>Temperatura</th>
			<th>Precipitación</th>
		</thead><tbody>";

foreach ($valores as $val){
	$header = $header."<tr><td>".$val['id']."</td>".
                        "<td>".$val['timestamp']."</td>".
                        "<td>".$val['ste']."</td>".
                        "<td>".$val['pat']."</td>".
                        "<td>".$val['vdv']."</td>".
                        "<td>".$val['ddv']."</td>".
                        "<td>".$val['hae']."</td>".
                        "<td>".$val['tae']."</td>".
                        "<td>".$val['pad']."</td></tr>";
}
$respuesta = $header."</tbody></table>";
echo $respuesta;

?>

