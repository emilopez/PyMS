<?php
include_once("conn.php");
include_once("funcionesdb.php");

$re = last();
$id = $re['id'];
$timestamp = $re['timestamp'];
$ste = $re['ste'];
$pat = $re['pat'];
$vdv = $re['vdv'];
$ddv = $re['ddv'];
$hae = $re['hae'];
$tae = $re['tae'];
$pad = $re['pad'];

$style = "<style type=\"text/css\">
#table-3 {
	border: 1px solid #DFDFDF;
	background-color: #F9F9F9;
	width: 100%;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
	font-family: Arial,\"Bitstream Vera Sans\",Helvetica,Verdana,sans-serif;
	color: #333;
}
#table-3 td, #table-3 th {
	border-top-color: white;
	border-bottom: 1px solid #DFDFDF;
	color: #555;
}
#table-3 th {
	text-shadow: rgba(255, 255, 255, 0.796875) 0px 1px 0px;
	font-family: Georgia,\"Times New Roman\",\"Bitstream Charter\",Times,serif;
	font-weight: normal;
	padding: 7px 7px 8px;
	text-align: left;
	line-height: 1.3em;
	font-size: 14px;
}
#table-3 td {
	font-size: 12px;
	padding: 4px 7px 2px;
	vertical-align: top;
}
</style>";

$header = $style."<table id=\"table-3\">
<thead>
<th colspan=\"9\"><b>Última captura de datos recibida</b></th>
</thead>
		<thead>
			<th>Id</th>
			<th>Datetime</th>
			<th>Sensación Térmica</th>
			<th>Presión atmosférica</th>
			<th>Velocidad del Viento</th>
			<th>Dirección del Viento</th>
			<th>Humedad</th>
			<th>Temperatura</th>
			<th>Precipitación</th>
		</thead><tbody>";

$respuesta = $header."<tr>
                        <td>$id</td>
                        <td>$timestamp</td>
                        <td>$ste</td>
                        <td>$pat</td>
                        <td>$vdv</td>
                        <td>$ddv</td>
                        <td>$hae</td>
                        <td>$tae</td>
                        <td>$pad</td>
                </tr></tbody></table>";
echo $respuesta;
#var_dump($respuesta);

?>

