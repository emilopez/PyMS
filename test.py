# -*- coding: iso-8859-15 -*-
from datetime import datetime
import glob
import os
import time
import logging
import requests
from pyms import Pegasus
import pyautogui
import pytesseract
import sys
import win32api

def click(x,y):
    win32api.SetCursorPos((x,y))
    win32api.mouse_event(0x0002,x,y,0,0)
    win32api.mouse_event(0x0004,x,y,0,0)



#from win32api import GetSystemMetrics
#width = GetSystemMetrics(0)
#hight = GetSystemMetrics(1)
#print(os.environ["PATH"])

os.environ["TESSDATA_PREFIX"]="C:\Documents and Settings\Administrador\Configuración local\Datos de programa\Tesseract-OCR"
os.environ["PATH"]="C:\Documents and Settings\Administrador\Anaconda;C:\Documents and Settings\Administrador\Anaconda\Scripts;C:\Documents and Settings\Administrador\Configuración local\Datos de programa\Tesseract-OCR"

#pyautogui.click(816,555)
dl = Pegasus()
click(816,555)
time.sleep(2)
dl.screenshot_box = (397, 330, 570, 400)
dl.screenshot()

dl.acron["ste"] = "Sensacin trmica"
dl.acron["ddv"] = "Direccin del viento"
dl.acron["tds"] = "Temperatura de suelo"
dl.acron["hae"] = "Humedad ambiente exterior"
dl.acron["idr"] = "Intensidad de rfaga"
dl.acron["tai"] = "Temperatura ambiente interior"
dl.acron["pur"] = "Punto de roco"
dl.acron["hds"] = "Humedad de suelo"
dl.acron["vdv"] = "Velocidad de viento"
dl.acron["pad"] = "Precipitacin acumulada diaria"
dl.acron["pat"] = "Presin atmosfrica"
dl.acron["tae"] = "Temperatura ambiente exterior"
dl.acron["hai"] = "Humedad ambiente interior"
dl.acron["ddr"] = "Direccin de rfaga"
dl.acron["ras"] = "Radiacin solar"


dl.variables_box = [ {"var": "tai", "box":(0,0,80,40)}, {"var": "pat", "box":(180,0,280,40)}, {"var": "hai", "box":(435,0,485,40)}, {"var": "tae", "box":(0,100,80,130)}, {"var": "hae", "box":(225,100,280,130)}, {"var": "pad", "box":(415,100,480,130)},{"var": "tds", "box":(0,185,80,220)}, {"var": "hds", "box":(225,185,280,220)}, {"var": "ras", "box":(415,185,480,220)},{"var": "ste", "box":(0,274,80,308)}, {"var": "ddv", "box":(180,274,340,308)}, {"var": "ddr", "box":(385,274,535,308)},{"var": "pur", "box":(0,360,80,395)}, {"var": "vdv", "box":(225,360,293,395)}, {"var": "idr", "box":(415,360,495,395)}]
dl.crop_variables()
dl.ocr_set_raw_variables()

try:
    
    dl.set_clean_data()
    dl.set_calibrate_data()

    dl.save_data()
    payload = dl.set_data_RTS()
    for k, v in payload.items():
        print(k,v)
    #r = requests.get("http://fca.yosobreip.com.ar/feed.php", params=payload)
    r = requests.get("http://www.fca.unl.edu.ar/estacion-meteorologica/feed.php", params=payload)
    logging.debug(str(r.status_code) + ' ' + r.text)
    print("To yosobreip:", str(r.status_code))

except Exception as e:
    logging.exception("message")
    print("error")


#a = input("ingresa")